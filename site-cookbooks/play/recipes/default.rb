#
# Cookbook Name:: play
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

#unzip インスコ
%w{unzip}.each do |pkg|
  package pkg do
    action :install
  end
end


user_name = "vagrant"
version = "2.2.4"
play_dir = "/home/vagrant/lib"

# playを置くディレクトリを作成
directory play_dir do
	owner user_name
	group user_name
	mode "0775"
action :create
end

cache_path = "/tmp"
zip_file_name = "play-#{version}.zip"
play_path = "#{play_dir}"

# remote_file:リモートサーバーにあるファイルをhttp経由で取得する命令
remote_file "#{cache_path}/#{zip_file_name}" do
	source "http://downloads.typesafe.com/play/#{version}/#{zip_file_name}"
	mode "0644"
end

bash "extract play zip" do
	cwd play_dir
	code <<-BASH
	  unzip #{cache_path}/#{zip_file_name}
	  chown -R #{user}:#{user} .
	BASH
end

bash "link play command to play_dir" do
	cwd play_dir
	code <<-BASH
	  ln -s #{play_path}/play-#{version}/play #{play_path}/play
	BASH
end

bash "add play command to bash" do
	code <<-BASH
	  echo "export PATH=#{play_path}:$PATH" >> /etc/bashrc
	BASH
	not_if "which play"
end